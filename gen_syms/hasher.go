package gen_syms

import "crypto/sha256"

type hasher struct{}

func (hasher) hash(domain string, master_pwd string) []byte {
	msg := domain + master_pwd
	fixed := sha256.Sum256([]byte(msg))
	return fixed[:]
}
