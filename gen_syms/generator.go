package gen_syms

import (
	"proto"
	"strings"
)

type generator struct {
	compiler compiler
	hasher   hasher
}

func make_alphabet() string {
	alphabet_low := "abcdefghijklmnopqrstuvwxyz"
	alphabet_high := strings.ToUpper(alphabet_low)
	digits := "0123456789"
	symbols := "!@#$%^&*()[]-_=+;:~?/"
	return alphabet_low + alphabet_high + digits + symbols
}

func NewGenerator() proto.IGenerator {
	return &generator{
		hasher:   hasher{},
		compiler: new_compiler(make_alphabet()),
	}
}

func (self *generator) Gen(domain string, master_pwd string, sizes []int) []string {
	bts := self.hasher.hash(domain, master_pwd)
	var result []string
	for _, size := range sizes {
		result = append(result, self.compiler.compile(bts, size))
	}
	return result
}
