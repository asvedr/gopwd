package gen_syms

type compiler struct {
	alphabet string
	size     int
}

func new_compiler(alphabet string) compiler {
	size := len(alphabet)
	return compiler{alphabet: alphabet, size: size}
}

func (self compiler) compile(code []byte, size int) string {
	var result []byte
	for _, code := range code[:size] {
		symbol := self.alphabet[int(code)%self.size]
		result = append(result, symbol)
	}
	return string(result)
}
