package proto

// type IHasher interface {
// 	Hash(domain string, master_pwd string) []byte
// }

type IPwdInput interface {
	GetPwd() (string, error)
}

type IGenerator interface {
	Gen(domain string, master_pwd string, sizes []int) []string
}

// type ITextCompiler interface {
// 	Compile(bts []byte, size int) string
// }
