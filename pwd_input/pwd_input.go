package pwd_input

import (
	"fmt"
	"proto"
	"syscall"

	"golang.org/x/term"
)

type pwd_input struct{}

func NewPwdInput() proto.IPwdInput {
	return &pwd_input{}
}

func (pwd_input) GetPwd() (string, error) {
	fmt.Printf("master password:")
	pwd_bts, err := term.ReadPassword(syscall.Stdin)
	fmt.Printf("\n")
	if err != nil {
		return "", fmt.Errorf("can not read pwd: %v", err)
	}
	return string(pwd_bts), err
}
