package gen_md5

import (
	"crypto/md5"
	"encoding/hex"
	"proto"
)

type generator struct{}

func NewGenerator() proto.IGenerator {
	return &generator{}
}

func (generator) Gen(domain string, master_pwd string, _ []int) []string {
	bts := md5.Sum([]byte(master_pwd + domain))
	digest := hex.EncodeToString(bts[:])
	return []string{digest}
}
