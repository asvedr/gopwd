package entrypoint

import (
	"fmt"
	"proto"

	"gitlab.com/asvedr/cli_run"
)

type UseCase struct {
	cli_run.BaseEntrypoint
	PwdInput proto.IPwdInput
	Md5Gen   proto.IGenerator
	SymsGen  proto.IGenerator
}

var Version string = "N/A"

var default_pwd_len = []int{32, 22, 12}

func (UseCase) Description() string {
	return fmt.Sprintf(
		"gen password for domain(version: %s)",
		Version,
	)
}

func (UseCase) Parser() cli_run.IArgParser {
	return new_parser()
}

func (self *UseCase) Execute(prog string, a_request any) error {
	request := a_request.(request)
	master, err := self.PwdInput.GetPwd()
	if err != nil {
		return err
	}
	switch request.gen_type {
	case gen_type_md5:
		self.md5_mode(master, request.domains)
	case gen_type_syms:
		self.syms_mode(master, request.domains, request.pwd_len)
	default:
		panic("invalid mode")
	}
	// bts := uc.Hasher.Hash(request.domain, master)
	// for _, size := range sizes {
	// 	pwd := uc.Compiler.Compile(bts, size)
	// 	fmt.Printf("Password(%d): %s\n", size, pwd)
	// }
	return nil
}

func (self *UseCase) md5_mode(
	master string,
	domains []string,
) {
	for _, domain := range domains {
		pwds := self.Md5Gen.Gen(domain, master, nil)
		fmt.Printf("%s: %s\n", domain, pwds[0])
	}
}

func (self *UseCase) syms_mode(
	master string,
	domains []string,
	pwd_len *int,
) {
	var sizes []int
	if pwd_len == nil {
		sizes = default_pwd_len
	} else {
		sizes = []int{*pwd_len}
	}
	for _, domain := range domains {
		for _, pwd := range self.SymsGen.Gen(domain, master, sizes) {
			fmt.Printf("%s(%d): %s\n", domain, len(pwd), pwd)
		}
	}
}
