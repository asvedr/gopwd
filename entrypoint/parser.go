package entrypoint

import (
	"gitlab.com/asvedr/cli_run"
)

type parser struct {
	generic cli_run.IGenericParser
}

type request struct {
	pwd_len  *int
	domains  []string
	gen_type gen_type
}

type gen_type int

const (
	gen_type_md5 gen_type = iota
	gen_type_syms
)

func new_parser() cli_run.IArgParser {
	prs := &parser{cli_run.GenericParser()}
	prs.generic.AddOptParam(
		[]string{"-p", "--pwd-len"},
		cli_run.Int{},
		"password len",
	)
	prs.generic.AddDefaultParam(
		[]string{"-t", "--gen-type"},
		cli_run.Str{
			Default: "md5",
			Choices: []string{"md5", "sym"},
		},
		"generator type",
	)
	prs.generic.SetRestType(&cli_run.Str{}, "domain")
	return prs
}

func (self parser) Parse(args []string) (any, error) {
	parsed, err := self.generic.Parse(args)
	if err != nil {
		return nil, err
	}
	a_pwd_len, found := parsed.Named["-p"]
	var pwd_len *int
	if found {
		as_int := int(a_pwd_len.(int64))
		pwd_len = &as_int
	}
	var domains []string
	for _, val := range parsed.Rest {
		domains = append(domains, val.(string))
	}
	var gt gen_type
	switch parsed.Named["-t"].(string) {
	case "md5":
		gt = gen_type_md5
	case "sym":
		gt = gen_type_syms
	default:
		panic("unexpected type")
	}
	return request{
		pwd_len:  pwd_len,
		domains:  domains,
		gen_type: gt,
	}, nil
}

func (self parser) Params() []string {
	return self.generic.IntoArgParser().Params()
}
