#!/bin/sh
VERSION=$(cat version.txt)
go build -ldflags="-X 'entrypoint.Version=$VERSION'"
