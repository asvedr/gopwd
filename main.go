package main

import (
	"entrypoint"
	"gen_md5"
	"gen_syms"
	"pwd_input"

	"gitlab.com/asvedr/cli_run"
)

func main() {
	uc := &entrypoint.UseCase{
		PwdInput: pwd_input.NewPwdInput(),
		SymsGen:  gen_syms.NewGenerator(),
		Md5Gen:   gen_md5.NewGenerator(),
	}
	cli_run.NewRunner().SetUseCase(uc).Run()
}
